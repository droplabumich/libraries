
#ifdef TARGET_LIKE_MBED
    #include "mbed.h"
    #include "mbed_printf/mbed_printf_implementation.h"
    #define snprintf mbed_snprintf
    #define sprintf mbed_sprintf
#elif ARDUINO_ARCH_AVR 
    #include"Arduino.h"
#endif

#ifdef ROS
    #include <cstring>
    #include <cstdlib>
    #include <cstdio>
#endif

#include <stdint.h>
#include <math.h>
#include <ctype.h>
#include "FPointer.h"
#include "Sensor_data.h"

#ifndef CommandInterface
#define CommandInterface

/*Commands: These commands should resemble the NMEA protocol 
General command structure: $<cmd_id>,<Data>,*<Checksum><CR><LF>

Available commands: 
1. ST:  Set thrusters: Send duty cycle for the Thrusters
        Format $ST,VAL_UL,VAL_UR,VAL_FL,VAL_FR*<Checksum><CR><LF>
        Request: AK Message
2. RD:  Request data. 
        Format $RD,<Data_Id>*<Checksum><CR><LF>
        Request: DM Message
3. RA:  Request all data
        Send Format $RA*<Checksum><CR><LF>
        Request: AD Message
4. AK:  Acknowledge message
        Send Format $AK,<Cmd_to_Acknowledge>*<Checksum><CR><LF>
        Request: None
5. DM   Data Message
        Send Format $DM,<Data_Id>,<Value>*<Checksum><CR><LF>
        Request: None
6. AD   All Data Message
        Send Format $AD,<Data_Id>,<Value>,<Data_Id>,<Value>,<Data_Id>,<Value>...*<Checksum><CR><LF>
        Request: None
7. SS   Sphere state message
        Send Format $SS,<Value>*<Checksum><CR><LF>
        Request: AK Message
8. UI   User Interface LEDS
        Send Format $UI,<Value>*<Checksum><CR><LF>
        Request AK Message
9. RL   Relay next message to board 
        Send Format $RL,<Target_Id>*<Checksum><CR><LF>        
*/

#ifdef TARGET_LIKE_MBED
	#define MAX_BUF_SIZE 512
   
#elif ARDUINO_ARCH_AVR 
        #define MAX_BUF_SIZE 128
#else 
        #define MAX_BUF_SIZE 512
#endif



class SerialProtocol {
    
    public:
        
        // Definition of the different message headers
        enum Cmd_Id {
            ST, 
            RD,
            RA,
            AK,
            DM,
            AD,
            SS,
            UI,
            RT,
            RL,
            NumberOfTypes,
        };
        
        static const char* rank_txt[];
               
        SerialProtocol(){};

        #ifdef TARGET_LIKE_MBED
        SerialProtocol(SENSOR_DATA::State* state, RawSerial *port);
        #endif
        SerialProtocol(SENSOR_DATA::State* state);
        virtual ~SerialProtocol();

        int lastMsgTypes[DATA_TYPES::NumberOfTypes]; 
        int redirectId; 

 
        
        /*virtual void onSt(float ul, float ur, float ll, float lr) = 0;
        virtual void onRT() = 0; 
        virtual void onRD() = 0;
        virtual void onRA() = 0; 
        virtual void onAK(int data_id_array[],float data_array[], int num_elem) = 0; 
        virtual void onDM(int data_id_array[],float data_array[], int num_elem) = 0;
        virtual void onAD(int data_id_array[],float data_array[], int num_elem) = 0; 
        virtual void onSS(int data_id_array[],float data_array[], int num_elem) = 0; 
        virtual void onUI(int data_id_array[],float data_array[], int num_elem) = 0; */
        // TODO: Add check to avoid creating msgs longer than incoming buffers
        char* createmsg(); 
        char* createmsg(Cmd_Id id, float data,int* length);
        char* createmsg(Cmd_Id id, int* length);
        char* createmsg(Cmd_Id id, float data_array[],int* length);
        char *createmsg(Cmd_Id id, int data_id, float data, int *length);
        char* createmsg(Cmd_Id id, const int id_data_array[], double data_array[], int n_elem, int* length); 
        char* createAD(int* message_length);
                
    
        char *lastMessage(void);
        bool newMessagereceived();
        bool parse(char *nmea); 
        void process_char(char c);


        template<class T> 
        void attach(int type, T* item, uint32_t (T::*method)(uint32_t)) { _callback_array[type]->attach(item, method); }
        void attach( int type, uint32_t (*function)(uint32_t) = 0 ) { _callback_array[type]->attach(function); }

    private:
        SerialProtocol(const SerialProtocol&);
        SerialProtocol& operator=(const SerialProtocol&);

        int numberOfCommasInArray(char* array);
        char *allocateBuffer(int bufferSizeNeeded);

        // Double buffer for storing incoming strings from the serial port and 
        // bookkeeping variables
        volatile bool recvdflag;
        volatile char line1[MAX_BUF_SIZE];
        volatile char line2[MAX_BUF_SIZE];
        volatile uint16_t lineidx;
        volatile char *currentline;
        volatile char *lastline;
        
        
        void reset_lastMsgTypes(void);
        
        
        bool parseST(char* nmea);
        bool parseRT(char* nmea);
        bool parseRD(char* nmea);  
        bool parseRA(char* nmea);  
        bool parseAK(char* nmea);  
        bool parseDM(char* nmea);  
        bool parseAD(char* nmea);  
        bool parseSS(char* nmea);  
        bool parseUI(char* nmea);  
        bool parseRL(char* nmea);  

        
        uint8_t parseHex(char c);
        
        // Serial port for debug output. Must not be the same as the serial port
        // the interface is working with.
        #ifdef TARGET_LIKE_MBED
        RawSerial* _port; 
        #endif

        // Buffer for outgoing size 
        char messageBuffer[MAX_BUF_SIZE];
        int bufferSize;
        
        // Function callbacks for reacting to incomming messages
        FPointer *    _callback_array[NumberOfTypes];
        
        
        SENSOR_DATA::State* _state;
};

#endif
