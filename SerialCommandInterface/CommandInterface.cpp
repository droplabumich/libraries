
#include "CommandInterface.h"

const char* SerialProtocol::rank_txt[] = {"$ST","$RD","$RA","$AK","$DM","$AD",
                                            "$SS","$UI"};



SerialProtocol::SerialProtocol (SENSOR_DATA::State* state) {
    lineidx=0;
    recvdflag = 0;
    currentline = line1;
    lastline    = line2;
    //messageBuffer= NULL;
    bufferSize  = 0;
    _state = state;
    redirectId = 0;
    for (int i =0; i<NumberOfTypes; i++) {
        _callback_array[i] = new FPointer();
        lastMsgTypes[i] = -1; // Not valid index 
    }

}

#ifdef TARGET_LIKE_MBED
SerialProtocol::SerialProtocol(SENSOR_DATA::State* state, RawSerial *port): _port(port) {
  lineidx=0;
    recvdflag = 0;
    currentline = line1;
    lastline    = line2;
    //messageBuffer= NULL;
    bufferSize  = 0;
    _state = state;
    redirectId = 0;
    for (int i =0; i<NumberOfTypes; i++) {
        _callback_array[i] = new FPointer();
        lastMsgTypes[i] = -1; // Not valid index 
    }
}
#endif

    
SerialProtocol::~SerialProtocol() {
    //if (messageBuffer!=NULL) {
    //    free(messageBuffer);
    //}
    for (int i =0; i<NumberOfTypes; i++) {
        delete _callback_array[i];
    }
}

    
void SerialProtocol::process_char(char c) {
    static bool parsingSentence = false;
    //_port->printf(" Got character %s ",c);
    if (c == '$') {
        parsingSentence = true;
        currentline[lineidx] = 0;
        lineidx = 0;
      //  _port->printf(" Newline! ");
    }
    if (c == '\n') {
        //_port->printf(" End of line! ");

        currentline[lineidx] = 0;
        lineidx = 0;
        
        if (parsingSentence) {
            if (currentline == line1) {
                currentline = line2;
                lastline = line1;
            } else {
                currentline = line1;
                lastline = line2;
            }
            
            recvdflag = true;
            parsingSentence = false;
        }
  }

  currentline[lineidx++] = c;
  if (lineidx >= MAX_BUF_SIZE)
    lineidx = MAX_BUF_SIZE-1;

  
}

bool SerialProtocol::newMessagereceived(void) {
  return recvdflag;
}

char *SerialProtocol::lastMessage(void) {
  recvdflag = false;
  return (char *)lastline;
}

bool SerialProtocol::parse(char *nmea) {
  // do checksum check
  //_port->printf("Parsing...\n\r");
  // first look if we even have one
  if (nmea[strlen(nmea)-4] == '*') {
    uint16_t sum = parseHex(nmea[strlen(nmea)-3]) * 16;
    sum += parseHex(nmea[strlen(nmea)-2]);
    
    // check checksum 
    for (uint16_t i=1; i < (strlen(nmea)-4); i++) {
      sum ^= nmea[i];
    }
    if (sum != 0) {
      // bad checksum :(
      //_port->printf("Bad checksum \n\r");
      return false;
    }
  }
 
    //If redirectId is set, this means the next message should be forwarded to the specified target
    // The redirect callback is responsible for setting redirectId to 0 again. 
    if (redirectId){
        _callback_array[RL]->call();
        return true;
    }

    //Look for command 
    if (strstr(nmea, "$RT")) parseRT(nmea);
    if (strstr(nmea, "$ST")) parseST(nmea);  
    if (strstr(nmea, "$RD")) parseRD(nmea);  
    if (strstr(nmea, "$RA")) parseRA(nmea);  
    if (strstr(nmea, "$AK")) parseAK(nmea);  
    if (strstr(nmea, "$DM")) parseDM(nmea);  
    if (strstr(nmea, "$AD")) parseAD(nmea);  
    if (strstr(nmea, "$SS")) parseSS(nmea);  
    if (strstr(nmea, "$UI")) parseUI(nmea);  
    if (strstr(nmea, "$RL")) parseRL(nmea);  
    
        
  return false;
}

bool SerialProtocol::parseRD(char* nmea) {
     
    // found GGA
    char *p = nmea;
    // get time
    p = strchr(p, ',')+1;
    int data_id = atoi(p);
    
    _callback_array[RD]->call((uint32_t) data_id);
    
    return true;  
}

bool SerialProtocol::parseRL(char* nmea) {
     
    // found GGA
    char *p = nmea;
    // get time
    p = strchr(p, ',')+1;
    redirectId = atoi(p);

    return true;  
}

bool SerialProtocol::parseST(char* nmea) {
    
    /*char *p = nmea;
    // get time
    p = strchr(p, ',')+1;
    //float thruster_upper_left = atof(p);
    _state->sensors[DATA_TYPES::pwm_ul].value = atof(p);
    p = strchr(p, ',')+1;
    _state->sensors[DATA_TYPES::pwm_ur].value= atof(p); 
    p = strchr(p, ',')+1;
    _state->sensors[DATA_TYPES::pwm_ll].value = atof(p);
    p = strchr(p, ',')+1;
    _state->sensors[DATA_TYPES::pwm_lr].value = atof(p);*/

    int ndata = numberOfCommasInArray(nmea)/2;
    char *p = nmea;
    //_port->printf("Number of AD data: %i",ndata);
    int data_id;
    for (int i = 0; i< ndata; i++) {
        p = strchr(p, ',') + 1;
        data_id = atoi(p);
        p = strchr(p, ',') + 1;
        if ((data_id < DATA_TYPES::NumberOfTypes) && (data_id >= 0)) {
            _state->sensors[data_id].value = atof(p);

        }
    }

    
    _callback_array[ST]->call();    
 
    return true;
 
}

void SerialProtocol::reset_lastMsgTypes(void) {
    for (int i = 0; i<DATA_TYPES::NumberOfTypes; i++) {
        lastMsgTypes[i] = -1;
    }
}

char *SerialProtocol::allocateBuffer(int bufferSizeNeeded) {
    /*     
    if (bufferSize < bufferSizeNeeded ) { // check if we already have a buffer big enough. If so keep it to save on freeing and allocating memory
      if (messageBuffer!=NULL) // buffer is too small
        free(messageBuffer);
     
      messageBuffer = (char*)malloc(bufferSizeNeeded * sizeof(char));
      if (messageBuffer == NULL) {  // FAILED TO ALLOCATE MEMORY...
        bufferSize = 0;
        return NULL;
      } else
      bufferSize  = bufferSizeNeeded;
    }  
     
    // buffer is now allocated */
    if (bufferSizeNeeded>MAX_BUF_SIZE) {
        //printf("Error: req buffer to long");
        return NULL; 
    } else {
        memset(messageBuffer, 0, MAX_BUF_SIZE); //Zeros the buffer 
        return messageBuffer;
    } 

      
      //memset(messageBuffer, 0, bufferSize); //Zeros the buffer 
      //return messageBuffer;
}

bool SerialProtocol::parseRT(char* nmea){
    _callback_array[RT]->call();
    return false;
}
    
bool SerialProtocol::parseRA(char* nmea){
    
    _callback_array[RA]->call();
    return true;
}
    
bool SerialProtocol::parseAK(char* nmea){
    
    char *p = nmea;
    // get time
    p = strchr(p, ',')+1;
    Cmd_Id cmd =(Cmd_Id) atoi(p);
    
    _callback_array[AK]->call((uint32_t) cmd);    
    
    return true;
}
    
bool SerialProtocol::parseDM(char* nmea){
    
    char *p = nmea;
    // get time
    p = strchr(p, ',')+1;
    int data_id = atoi(p);
    p = strchr(p, ',')+1;
    reset_lastMsgTypes();
    if (data_id < DATA_TYPES::NumberOfTypes) {
        _state->sensors[data_id].value = atof(p); 
        lastMsgTypes[0] = data_id;
    }
    
    _callback_array[DM]->call((uint32_t) data_id);
    return true;
}
    
bool SerialProtocol::parseAD(char* nmea){
    int ndata = numberOfCommasInArray(nmea)/2;
    char *p = nmea;
    //_port->printf("Number of AD data: %i",ndata);
    int data_id;
    reset_lastMsgTypes();

    for (int i = 0; i< ndata; i++) { 
        p = strchr(p, ',')+1;
        data_id = atoi(p);
        p = strchr(p, ',')+1;
        if (data_id < DATA_TYPES::NumberOfTypes) {
            _state->sensors[data_id].value = atof(p);
            lastMsgTypes[i] = data_id;    // TODO: This could lead to a array with -1 elements between valid values. Find better way

        }
        //_port->printf("Data id %i \n\r",data_id_array[i]);
        //_port->printf("Data cont %f \n\r",data_array[i]);

    }
    
    _callback_array[AD]->call();
    return true;
}
    
bool SerialProtocol::parseSS(char* nmea){
    char *p = nmea;
    p = strchr(p, ',')+1;
    reset_lastMsgTypes();

    _state->sensors[DATA_TYPES::status].value = atoi(p);
    lastMsgTypes[0] = DATA_TYPES::status; 

    _callback_array[SS]->call();    
    
    return true;
}
    
bool SerialProtocol::parseUI(char* nmea){
    char *p = nmea;
    p = strchr(p, ',')+1;
    _state->sensors[DATA_TYPES::user_iface].value = atoi(p);
    lastMsgTypes[0] = DATA_TYPES::user_iface; 

    _callback_array[UI]->call();
    return true;
}

uint8_t SerialProtocol::parseHex(char c) {
    if (c < '0')
      return 0;
    if (c <= '9')
      return c - '0';
    if (c < 'A')
       return 0;
    if (c <= 'F')
       return (c - 'A')+10;
}

char *SerialProtocol::createmsg(Cmd_Id id, float data, int* length) {    
    // This function creates messages of type RD, AK, SS, UI
     
    int len = snprintf(NULL, 0, "%.5f", data);
    uint16_t sum = 0; 
    
    int total_length = len+8;
    
    char* buf = allocateBuffer(total_length); 
    *length = total_length;
    
    if (buf == NULL) { // Could not allocate buffer
        //_port->printf("Could not allocate buffer \n\r");
        return NULL; 
    }
    
    memcpy(buf,rank_txt[id],3);  //CMD_ID
    memcpy(buf+3,",",1);         // , 
    sprintf(buf+4,"%.5f", data); // <DATA>
    for (uint8_t i=1; i < (len+3); i++) {
      sum ^= buf[i];
    }
    memcpy(buf+3+len,"*",1);     // *
    sprintf(buf+len+4,"%02X",sum); // <Checksum>
    sprintf(buf+len+6,"\r\n");   // <CR><LF>
    
    
    //_port->printf("Desired length %i \n\r", len);
    //_port->printf("Buffer content: %s ",buf);
    //for (int i = 0; i < len+8; i++)
    //{    _port->printf("%02X", buf[i]);}
    
    
    return buf; 
}

char *SerialProtocol::createmsg(Cmd_Id id, int* length) {    
    // This function creates messages of type RA
     
    uint16_t sum = 0; 
    
    char* buf = allocateBuffer(9); 
    *length = 9;
    
    if (buf == NULL) { // Could not allocate buffer
        //_port->printf("Could not allocate buffer \n\r");
        return NULL; 
    }
    
    memcpy(buf,rank_txt[id],3);  //CMD_ID
    for (uint8_t i=1; i < 3; i++) {
      sum ^= buf[i];
    }
    memcpy(buf+3,"*",1);     // *
    sprintf(buf+4,"%02X",sum); // <Checksum>
    sprintf(buf+6,"\r\n");   // <CR><LF>
    
    return buf; 
}

char *SerialProtocol::createmsg(Cmd_Id id, int data_id, float data, int *length) {
    uint16_t sum = 0;
    int total_length = 8;
    char *buf = allocateBuffer(total_length);

    memcpy(buf, rank_txt[id], 3); //CMD_ID
    if (buf == NULL)
    { // Could not allocate buffer
        //_port->printf("Could not allocate buffer \n\r");
        return NULL;
    }
    int len_data = snprintf(NULL, 0, "%.7f", data);
    int len_id = snprintf(NULL, 0, "%i", data_id);
    total_length += (len_data + len_id);

    memcpy(buf, rank_txt[id], 3);                       //CMD_ID
    memcpy(buf + 3 , ",", 1);                           // ,
    sprintf(buf + 4, "%i", data_id);                 // <DATA_ID>
    memcpy(buf + 4  + len_id, ",", 1);                  // ,
    sprintf(buf + 5 + len_id, "%.7f", data);   // <DATA>
    for (int i = 1; i < (total_length - 5); i++)
    {
        sum ^= buf[i];
    }
    memcpy(buf + total_length - 5, "*", 1);       // *
    sprintf(buf + total_length - 4, "%02X", sum); // <Checksum>
    sprintf(buf + total_length - 2, "\r\n");      // <CR><LF>
    *length = total_length;

    return buf;
}

char *SerialProtocol::createmsg(Cmd_Id id, const int id_data_array[], double data_array[], int num_elem, int* length) {

    uint16_t sum = 0; 
    
    int len_data_arr[num_elem]; 
    int len_id_arr[num_elem]; 
    
     int total_length = num_elem+8;
     for (int j = 0; j<num_elem; j++) {    
            len_data_arr[j] =  snprintf(NULL, 0, "%.7f", data_array[j]);
            len_id_arr[j] =  snprintf(NULL, 0, "%i", id_data_array[j]);   
            total_length += (len_data_arr[j]+len_id_arr[j]);
    } 
   
   //total_length = 163;
    
    char* buf = allocateBuffer(total_length); 
    *length = total_length;
    //printf("Total length computed: %i \n\r", total_length);
    if (buf == NULL) { // Could not allocate buffer
        printf("Could not allocate buffer \n\r");
        return NULL; 
    }
    
     memcpy(buf,rank_txt[id],3);  //CMD_ID
    int last_index = 0;  
    // Fill message with contents
    for (int j = 0; j<num_elem; j++) {
        memcpy(buf+3+last_index, ",",1);                        // ,
        sprintf(buf+4+last_index,"%i", id_data_array[j]);        // <DATA_ID>
        memcpy(buf +4+last_index+ len_id_arr[j],",",1);                   // ,
        sprintf(buf +5+last_index+ len_id_arr[j],"%.7f", data_array[j]);  // <DATA>
        last_index += (1+len_id_arr[j]+len_data_arr[j]); 
    }
    
    //_port->printf("Checksum computed on a length of %i: ",total_length-5);
    for (int i=1; i < (total_length - 5); i++) {
      //_port->printf("i: %i B_val: %02X \n\r", i, buf[i]);
      sum ^= buf[i];
    }
    //_port->printf(" Done\n\r");
    
    memcpy(buf+total_length-5,"*",1);     // *
    sprintf(buf+total_length-4,"%02X",sum); // <Checksum>
    sprintf(buf+total_length-2,"\r\n");   // <CR><LF>
    

    //_port->printf("Number of elements %i \n\r", num_elem);
    //_port->printf("Desired length %i \n\r", total_length);
    //_port->printf("Buffer content: %s ",buf);
    //for (int i = 0; i < total_length; i++)
    //{    _port->printf("%02X", buf[i]);}
    
    
    return buf; 
}

char* SerialProtocol::createAD(int* message_length) {
    int id_data_array[DATA_TYPES::NumberOfTypes];
    double data_array[DATA_TYPES::NumberOfTypes];
    for (int i=0; i < DATA_TYPES::NumberOfTypes; i++) {
        id_data_array[i] = i;
        data_array[i] = _state->sensors[i].value;
    }

    return createmsg(SerialProtocol::AD, id_data_array, data_array,
     DATA_TYPES::NumberOfTypes, message_length);
}

int SerialProtocol::numberOfCommasInArray(char* array){
 int i;
 for (i=0; array[i]; array[i]==',' ? i++ : *array++) 
 {}

return i;
}



