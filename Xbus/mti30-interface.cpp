/*#include "mti30-interface.h"

 MTI30::MTI30(RawSerial *serial_port){
    iface = serial_port;
    xbusCallback = {};
    xbusCallback.allocateBuffer = allocateMessageData;
    //xbusCallback.deallocateBuffer = deallocateMessageData;
    //xbusCallback.handleMessage = mtMessageHandler;

    xbusParser = XbusParser_create(&xbusCallback);

}

 void MTI30::sendMessage(XbusMessage const* m )
{
    uint8_t buf[64];
    size_t rawLength = XbusMessage_format(buf, m, XLLF_Uart);
    for (size_t i = 0; i < rawLength; ++i)
    {
        iface->putc(buf[i]);
    }
}


 void MTI30::mtMessageHandler(struct XbusMessage const* message)
{
    XbusMessage* m = g_messagePool.alloc();
    if (m)
    {
        memcpy(m, message, sizeof(XbusMessage));
        if (message->mid == XMID_MtData2)
        {
            g_dataQueue.put(m);
        }
        else
        {
            g_responseQueue.put(m);
        }
    }
    else if (message->data)
    {
        deallocateMessageData(message->data);
    }
}

// Allocate message data buffer from the message data pool.
void* allocateMessageData(size_t bufSize)
{
    return bufSize < MAX_XBUS_DATA_SIZE ? g_messageDataPool.alloc() : NULL;
}

// Deallocate message data previously allocated from the message data pool. 
void deallocateMessageData(void const* buffer)
{
    g_messageDataPool.free((uint8_t(*)[MAX_XBUS_DATA_SIZE])buffer);
}

XbusMessage const* MTI30::doTransaction(XbusMessage const* m)
{
    sendMessage(m);

    osEvent ev = g_responseQueue.get(500);
    return ev.status == osEventMessage ? (XbusMessage*)ev.value.p : NULL;
}

// Set the IMU into measurement mode 
 bool MTI30::streamMeasurements() {
    XbusMessage goConf = {XMID_GotoMeasurement};
    XbusMessage const* didRsp_conf = doTransaction(&goConf);
    if (didRsp_conf) 
        return true;
    else
        return false;
}

// Set the IMU into Configuration mode 
 bool MTI30::enterConfig() {
    XbusMessage goConf = {XMID_GotoConfig};
    XbusMessage const* didRsp_conf = doTransaction(&goConf);
    if (didRsp_conf) 
        return true;
    else
        return false;
}

//Read the device ID of the motion tracker.
/*uint32_t MTI30::readDeviceId(void)
{
    enterConfig();
    XbusMessage reqDid = {XMID_ReqDid};
    XbusMessage const* didRsp = doTransaction(&reqDid);
    XbusMessageMemoryManager janitor(didRsp,g_messagePool);
    uint32_t deviceId = 0;
    if (didRsp)
    {
        if (didRsp->mid == XMID_DeviceId)
        {
            deviceId = *(uint32_t*)didRsp->data;
        }
    }
    else {
        deviceId = 256;
    }
    streamMeasurements();
    return deviceId;

}*/
/*
void MTI30::printMessageData(struct XbusMessage const* message, RawSerial &iface)
{
    if (!message)
        return;

    iface.printf("MTData2:");
    uint16_t counter;
    if (XbusMessage_getDataItem(&counter, XDI_PacketCounter, message))
    {
        iface.printf(" Packet counter: %5d", counter);
    }
    float ori[4];
    if (XbusMessage_getDataItem(ori, XDI_Quaternion, message))
    {
        iface.printf(" Orientation: (% .3f, % .3f, % .3f, % .3f)", ori[0], ori[1],
                ori[2], ori[3]);
    }
    float acc[3];
    if (XbusMessage_getDataItem(acc, XDI_Acceleration, message))
    {
        iface.printf(" Acceleration: (% .3f, % .3f, % .3f)", acc[0], acc[1], acc[2]);
    }
    float gyr[3];
    if (XbusMessage_getDataItem(gyr, XDI_RateOfTurn, message))
    {
        iface.printf(" Rate Of Turn: (% .3f, % .3f, % .3f)", gyr[0], gyr[1], gyr[2]);
    }
    float mag[3];
    if (XbusMessage_getDataItem(mag, XDI_MagneticField, message))
    {
        iface.printf(" Magnetic Field: (% .3f, % .3f, % .3f)", mag[0], mag[1], mag[2]);
    }
    float euler[3];
    if (XbusMessage_getDataItem(euler, XDI_EulerAngles, message))
    {
        iface.printf(" Euler Angles: (% .3f, % .3f, % .3f)", euler[0], euler[1], euler[2]);
    }
    float temp; 
    if (XbusMessage_getDataItem(&temp, XDI_Temperature, message))
    {
        iface.printf(" Temperature: % .3f,", temp);
    }
    uint32_t status;
    if (XbusMessage_getDataItem(&status, XDI_StatusWord, message))
    {
        iface.printf(" Status:%X", status);
    }
    iface.printf("\r\n");
}*/