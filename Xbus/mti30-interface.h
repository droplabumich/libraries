#ifndef __MTI30INTERFACE_H
#define __MTI30INTERFACE_H

#include "mbed.h"

#include "xbusparser.h"
#include "xbusmessage.h"
#include "xsdeviceid.h"
#include "xbusdef.h"



#define MEMORY_POOL_SIZE (4)
#define RESPONSE_QUEUE_SIZE (1)
#define DATA_QUEUE_SIZE (2)
#define MAX_XBUS_DATA_SIZE (128)

MemoryPool<XbusMessage, MEMORY_POOL_SIZE> g_messagePool;
MemoryPool<uint8_t[MAX_XBUS_DATA_SIZE], MEMORY_POOL_SIZE> g_messageDataPool;
//Queue used to pass data messages to the main thread for processing.
	Queue<XbusMessage, DATA_QUEUE_SIZE> g_dataQueue;
	// Queue used for passing all other messages to the main thread for processing.
	Queue<XbusMessage, RESPONSE_QUEUE_SIZE> g_responseQueue;

void* allocateMessageData(size_t bufSize);
void deallocateMessageData(void const* buffer);

class MTI30 
{

public:

    MTI30(RawSerial *serial_port); 

    void printMessageData(struct XbusMessage const* message, RawSerial &iface);
	uint32_t readDeviceId(void);
	XbusParser* xbusParser;
	//void* allocateMessageData(size_t bufSize);
	//void deallocateMessageData(void const* buffer);

protected:
  	void sendMessage(XbusMessage const* m );
	XbusMessage const* doTransaction(XbusMessage const* m);
	
	bool streamMeasurements();
	bool enterConfig();
	void mtMessageHandler(struct XbusMessage const* message);


	//Memory pool used for storing Xbus messages when passing them
	// Memory pool used for storing the payload of Xbus messages.
	

	XbusParserCallback xbusCallback;
	RawSerial* iface; 
};

/*class XbusMessageMemoryManager
{
    public:
        XbusMessageMemoryManager(XbusMessage const* message, MemoryPool* pool)
            : m_message(message)
        {
        	g_mPool = pool;
        }

        ~XbusMessageMemoryManager()
        {
            if (m_message)
            {
                if (m_message->data)
                    MTI30::deallocateMessageData(m_message->data);
                g_mPool->free(const_cast<XbusMessage*>(m_message));
            }
        }

    private:
        XbusMessage const* m_message;
        MemoryPool<XbusMessage, MEMORY_POOL_SIZE>* g_mPool;

};*/

#endif // __MTI30INTERFACE_H
