#ifdef TARGET_LIKE_MBED
    #include "mbed.h"
#elif ARDUINO_ARCH_AVR 
    #include"Arduino.h"
#endif

#ifndef Sensor_data
#define Sensor_data


namespace DATA_TYPES {
    
enum DataId {
    temp_int_hih,           // In degree Celsius
    temp_int_mpl,           // In degree Celsius
    temp_int_xsens,         // In degree Celsius
    temp_ext,               // In degree Celsius
    press_int,              // In Pascals  
    press_ext,              // In Pascals  
    humidity,               // In percent
    current_3V,             // In mA 
    current_5V,             // In mA
    current_bat,            // In mA
    rbat_voltage,           // In V
    r5V_voltage,            // In V 
    r3V_voltage,            // In V
    latitude,               // In degrees
    longitude,              // In degrees
    status,                 // Int 
    pwm_ul,                 // Setpoint, between 0 and 1
    pwm_ur,                 // Setpoint, between 0 and 1
    pwm_fr,                 // Setpoint, between 0 and 1
    pwm_fl,                 // Setpoint, between 0 and 1
    user_iface,             // Int 
    roll,                   // In radians 
    pitch,                  // In radians
    yaw,                    // In radians
    accel_x,                // In m/s2
    accel_y,                // In m/s2
    accel_z,                // In m/s2
    altitude,               // In meters 
    cam_voltage,            // In V 
    cam_current,            // In mA 
    altimeter_voltage,      // In V 
    altimeter_current,      // In mA 
    current_time,           // In s since epoch .secondfraction 
    imu_timestamp,          // In s since epoch .secondfraction 
    gyro_x, 
    gyro_y,
    gyro_z,
    cameraTriggerEnable,    // Enable (=1) or disable (=0) the camera trigger 

    NumberOfTypes,
};

enum VarTypes{
    float_t, 
    double_t, 
    int_t,
}; 

}



struct PairValue {
    DATA_TYPES::DataId id; 
    DATA_TYPES::VarTypes type; 
};

namespace SENSOR_DATA{
class Data{
    
    public:
        Data();
        virtual ~Data();
        Data(DATA_TYPES::DataId val_id, double val);
        //char raw_data[7];
        double value;         
        DATA_TYPES::DataId id;
};


class State {
   
public: 
    State(); 

    Data sensors[DATA_TYPES::NumberOfTypes];


//private:
    //Data sensors[DATA_TYPES::NumberOfTypes];

};

}
//state.sensors[DATA_TYPES::pwm_ul].value)
#endif

