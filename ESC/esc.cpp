#include "mbed.h"
#include "esc.h"

ESC::ESC(const PinName pwmPinOut, const bool reversed, const int period)
        : esc(pwmPinOut), period(period), throttle(1000), _reversed(reversed)
{
    esc.period_ms(period);
    esc.pulsewidth_us(throttle);
}

bool ESC::setThrottle (const float t)
{   
    if (t >= 0.0 && t <= 1.0) {
        if (_reversed) {
            throttle = 2000.0-1000.0*t;
        }
        else {
            throttle = 1000.0*t + 1000;     // map to range, 1-2 ms (1000-2000us)
            return true;
        }
           // qualify range, 0-1    
    }
    return false;
}
bool ESC::operator= (const float t){
    return this->setThrottle(t);
}

float ESC::getThrottle () const{
    return throttle;
}
ESC::operator float () const{
    return this->getThrottle();
}

void ESC::pulse ()
{
    esc.pulsewidth_us(throttle);
}
void ESC::operator() ()
{
    this->pulse();
}