/*
  @file HIH6130.h
  
  @brief Humidity and Temperature Sensor HIH6130 Breakout I2C Library      

  @Author spiridion (http://mailme.spiridion.net)

  Tested on LPC1768 and FRDM-KL25Z
  
  Copyright (c) 2014 spiridion, 2017 University of Michigan
  Released under the MIT License (see http://mbed.org/license/mit)

  Documentation regarding I2C communication with HIH6130 can be found here: 
  http://mbed.org/media/uploads/spiridion/i2c_comms_humidicon_tn_009061-2-en_final_07jun12.pdf
*/

#ifndef HIH6130_H
#define HIH6130_H

#include "mbed.h"

///  default address is not 0X27, as stated in the documentation, but 0x4E (0x27<<1 ?) 
#define HIH6130_I2C_ADDRESS 0x4E 

#define UNSET_HI6130_HUMIDITY_VALUE -100.F
#define UNSET_HI6130_TEMPERATURE_VALUE -273.15F // absolute zero

/** HIH6130 class.
 *  Read humidity and temperature from the HIH6130 Breakout I2C sensor
 *
 * Example:
 * @code
 * #include "mbed.h"
 * #include "HIH6130.h"
 * 
 * #if defined(TARGET_LPC1768)
 *     #define PIN_SDA p9
 *     #define PIN_SCL p10
 * #elif defined(TARGET_KL25Z) // watch out for the PTE0/PTE1 mixed up in the KL25Z doc 
 *     #define PIN_SDA PTE0
 *     #define PIN_SCL PTE1
 * #endif
 * 
 * int main() 
 * {    
 *     HIH6130 hih6130(PIN_SDA, PIN_SCL);
 *     float humidity, temperature;
 *     
 *     while(1) {        
 *         if (hih6130.ReadData(&humidity, &temperature))
 *             printf("Humidity(%%RH): %8.2f \t Temperature(C): %8.2f\n", humidity, temperature);  
 *         wait(1);
 *     }
 * }
 * @endcode
 */
class HIH6130 
{

public:

    /** Create a HIH6130 instance
     * @param sda pin 
     * @param scl pin 
     * @param address: I2C slave address 
     */
    //HIH6130(PinName sda, PinName scl, int address = HIH6130_I2C_ADDRESS); 

    /** Create a HIH6130 instance
     * @param i2c object
     * @param address: I2C slave address 
     */
    HIH6130(I2C *i2c, int address = HIH6130_I2C_ADDRESS, RawSerial *pc = NULL); 

    /** Read relative humidity and temperature from the HIH6130.
     * @param humidity (%RH) 
     * @param temperature (C) 
     * @returns
     *   1 on success,
     *   0 on error
     */    
    int ReadData(double* pTemperature=NULL, double* pHumidity=NULL);

    /** Get temperature from a previous measurement 
     *  
     * @returns
     *   temperature (C)
     */    
    double GetTemperature() {return m_temperature;}

    /** Get relative humidity from a previous measurement
     *
     * @returns
     *   relative humidity (%RH)
     */    
    double GetHumidity() {return m_humidity;}; 

protected:
    void debugOut(const char * format, ...);

    I2C* m_i2c;   
    int m_addr;  
    RawSerial *_debug;     

    /** Measurement request and data fetch
     */    
    int Measurement();

    /** Calculation of the temperature from the digital output
     */    
    double TrueTemperature();
    
    /** Calculation of the humidity from the digital output
     */    
    double TrueHumidity();
    
    double m_temperature;  
    double m_humidity;  

     
    char m_data[8];    

};

#endif
