/*
  @file HIH6130.cpp
  
  @brief Humidity and Temperature Sensor HIH6130 Breakout I2C Library      

  @Author spiridion (http://mailme.spiridion.net)

  Tested on LPC1768 and FRDM-KL25Z
  
  Copyright (c) 2014 spiridion, 2017 University of Michigan
  Released under the MIT License (see http://mbed.org/license/mit)

  Documentation regarding I2C communication with HIH6130 can be found here: 
  http://mbed.org/media/uploads/spiridion/i2c_comms_humidicon_tn_009061-2-en_final_07jun12.pdf
*/
 
#include "HIH6130.h"
#include "mbed.h"
#include "stdarg.h" // For debugOut use of the ... parameter and va_list

/*HIH6130::HIH6130(PinName sda, PinName scl, int address) : m_i2c(sda,scl), m_addr(address)
{
    m_temperature = UNSET_HI6130_TEMPERATURE_VALUE;
    m_humidity = UNSET_HI6130_HUMIDITY_VALUE;
}*/

HIH6130::HIH6130(I2C* i2c, int address, RawSerial *pc) : m_i2c(i2c), m_addr(address), _debug(pc)
{
    m_temperature = UNSET_HI6130_TEMPERATURE_VALUE;
    m_humidity = UNSET_HI6130_HUMIDITY_VALUE;
    m_data[0]= 0x00;
    m_data[1]= 0x00;
    m_data[2]= 0x00;
    m_data[3]= 0x00;
   
    


}

int HIH6130::ReadData(double* pTemperature, double* pHumidity)
{
    int rsl = Measurement();
    
    if (rsl)
    { 
        m_temperature = TrueTemperature();
        m_humidity = TrueHumidity();
        debugOut("Success reading HIH6130.\n\r");

    }
    else
    {
        m_temperature = UNSET_HI6130_TEMPERATURE_VALUE;
        m_humidity = UNSET_HI6130_HUMIDITY_VALUE;
        debugOut("Failed reading HIH6130.\n\r");
    }
    
    if (pTemperature)
        *pTemperature = m_temperature;
    if (pHumidity)
        *pHumidity = m_humidity;

    return rsl;
}

double HIH6130::TrueTemperature()
{   
    // T = T_output / (2^14-2)*165-40
    return ( ( ((unsigned int)m_data[2] << 8) | (unsigned int)m_data[3] ) >> 2 ) * 0.010072F - 40;  


}

double HIH6130::TrueHumidity()
{
    // H = H_output /(2^14-2)*100
    return ( (((unsigned int)m_data[0] & 0x3f) << 8) | ((unsigned int)m_data[1] & 0xff) ) * 0.006104F;
}

int HIH6130::Measurement()
{
    int errors;

    // Humidity and temperature measurement request
    errors = m_i2c->write(m_addr, m_data, 1);
    //errors = m_i2c->write(m_addr);
    wait_ms(2);
    
    // Humidity and temperature data fetch
    errors += m_i2c->read(m_addr, m_data, 4);

    //  Check data validity
    if ( errors || !(m_data[0] & 0xC0)) {
        return 0;
    }
    else {
        //_debug->printf("HIH6130 m_data: 0x%02X:%02X:%02X:%02X \n\r",(unsigned char)m_data[0],(unsigned char)m_data[1],(unsigned char)m_data[2],(unsigned char)m_data[3]);
        return 1;
    }
}

void HIH6130::debugOut(const char * format, ...)
{
    if (_debug == NULL)
        return;
        
    va_list arg;
    _debug->printf(format, arg);
}