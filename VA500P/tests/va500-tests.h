
#ifndef _VA500D_TEST_H_INCLUDED 
#define _VA500_TEST_H_INCLUDED

#include "va500.h"
#include <chrono>
#include <thread>

class VA500P_TESTCLASS: public VA500P {
public:
    VA500P_TESTCLASS(): VA500P() {

    };

    bool sendCmd(char * data) {
        return true;
    };

    char* getOutputBuffer() {
        char* output = outputBuffer; 
        return output;
    }
    
    char* getLastBuffer() {
        char* output = (char*) lastBuffer;         
        return lastBuffer;
    }

    bool receiveStr() {
        return true;
    }

    void wait_ms(int milisecs) {
        std::this_thread::sleep_for(std::chrono::milliseconds(milisecs));;
    };
};

#endif