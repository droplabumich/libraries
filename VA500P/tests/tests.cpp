#include "va500-tests.h"
#include <gtest/gtest.h>
#include <iostream>

TEST(setSoundSpeed, right) {
    VA500P_TESTCLASS va500;
    const char teststr[] =">#830;1500.50\n1500.50\n";
    int length = sizeof(teststr)/sizeof(char);
    for (int i=0;i<length; i++){
        va500.addChar(teststr[i]);
    }
    ASSERT_TRUE(va500.setSoundSpeed(1500.50));
    ASSERT_STREQ("#830;1500.50\n",va500.getOutputBuffer());
    
}

TEST(processStr, validInput) {
    VA500P_TESTCLASS va500;
    char teststr[] = "$4000\n";
    for (int i=0;i<6; i++){
        va500.addChar(teststr[i]);
    }
    ASSERT_STREQ(teststr,va500.getLastBuffer());    
    ASSERT_TRUE(va500.newMessageReceived);
}

TEST(setRangeUnits, validInput) {
    VA500P_TESTCLASS va500;
    va500.setRangeUnits(RangeUnits::meters);
    ASSERT_STREQ("#021;1\n",va500.getOutputBuffer());    
    
}

TEST(processStr, right) {
    VA500P_TESTCLASS va500;
    char teststr[] = "1234\n";
    for (int i=0;i<6; i++){
        va500.addChar(teststr[i]);
    }
    ASSERT_FALSE(va500.processBuffer());
    char teststr2[] = "$14789\n";
    for (int i=0;i<7; i++){
        va500.addChar(teststr2[i]);
    }
    ASSERT_STREQ(teststr2,va500.getLastBuffer());
    ASSERT_TRUE(va500.newMessageReceived);    
}

TEST(processStr, parseNMEA) {
    VA500P_TESTCLASS va500;
    char teststr[] = "$PRVAT,00.115,M,0010.073,dBar*39\n";
    int length = sizeof(teststr)/sizeof(char);
    for (int i=0;i<length; i++){
        va500.addChar(teststr[i]);
    }
    ASSERT_TRUE(va500.newMessageReceived);
    va500.processBuffer();
    ASSERT_FLOAT_EQ(va500.getDepth(),0.115);
    ASSERT_FLOAT_EQ(va500.getAltitude(),10.073);
    ASSERT_TRUE(va500.processBuffer());
    
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
