#ifndef _VA500DESKTOP_H_INCLUDED 
#define _VA500DESKTOP_H_INCLUDED

#include "va500.h"
#include "mbed.h"



class mbedVA500P: public VA500P {
public:
    mbedVA500P(RawSerial* serialPort,
        RangeUnits::RangeUnits rangeUnit=RangeUnits::meters, 
        PressureUnits::PressureUnits pressureUnit=PressureUnits::meters):
         VA500P(rangeUnit,pressureUnit), port(serialPort) {};
 
    bool sendCmd(char * data) {
        port->puts(data);
        return true;
    };

    void wait_ms(int milisecs) {
        ThisThread::sleep_for(milisecs);
    };

    char* getLastBuffer() {
        return lastBuffer;
    }

    RawSerial* port;
};

#endif