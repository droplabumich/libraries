
#ifndef _VA500DESKTOP_H_INCLUDED 
#define _VA500DESKTOP_H_INCLUDED

#include "va500.h"
#include <chrono>
#include <thread>
#include "Serial.h"
#include <cstring>
#include <iostream>

class desktopVA500P: public VA500P {
public:
    desktopVA500P(const char* portName, int baudrate,
        RangeUnits::RangeUnits rangeUnit=RangeUnits::meters, 
        PressureUnits::PressureUnits pressureUnit=PressureUnits::meters):
         VA500P(rangeUnit,pressureUnit){
            port.connect(portName, baudrate);
         };

    ~desktopVA500P(){
        port.disconnect();
    }
    
    bool sendCmd(char * data) {
        std::cout<< "Wrote: " << data<<std::endl;
        port.write(data,strlen(data));
        return true;
    };

    void wait_ms(int milisecs) {
        std::this_thread::sleep_for(std::chrono::milliseconds(milisecs));;
    };

  
    char* getLastBuffer() {
        char* output = (char*) lastBuffer;         
        return lastBuffer;
    }
    SerialPort port; 
    
protected:
    
    int baudrate; 

};

#endif