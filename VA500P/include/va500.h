

#ifndef _VA500_H_INCLUDED 
#define _VA500_H_INCLUDED 

#include "va500-cmds.h"
//#include "stdint.h"
const int INPUT_BUFLEN = 128; 
const int OUTPUT_BUFLEN = 24; 

namespace RangeUnits {
    enum RangeUnits {
        seconds, 
        meters,
        feet,
        fathoms,
    };

    const char range_literals[4][2]={"S", "M", "f", "F"}; 
}

namespace PressureUnits {
    enum PressureUnits {
        dBar,
        meters,
        feet,
    };

    const char pressure_literals[3][7]={"dBar", "Metres", "feet"}; 
    
}


enum UnitMode {
    streaming,
    serialTrigger,
    ttlTrigger,
    config,
    uninit,
};

class VA500P {
public:
    VA500P(RangeUnits::RangeUnits rangeUnit=RangeUnits::meters, 
        PressureUnits::PressureUnits pressureUnit=PressureUnits::meters);

    float getDepth();
    float getAltitude();
   
    bool setSoundSpeed(float soundSpeed);
    bool setRangeUnits(RangeUnits::RangeUnits unit);
    bool setPressureUnits(PressureUnits::PressureUnits unit);
    bool setTare();
    bool setFrequency(int hz);

    void addChar(char data);

    virtual bool sendCmd(char * data) = 0;
    virtual void wait_ms(int milisecs) = 0;
    
    bool newMessageReceived; 
    
    bool processBuffer();
    int calcchecksum(char* array);
    bool setup();
    
protected:

    RangeUnits::RangeUnits _rangeUnit; 
    PressureUnits::PressureUnits _pressureUnit; 

    float min_range;
    float max_range; 

    float altitude; 
    float depth; 
    float soundSpeed; 
    float tare; 

    char buffer_a[INPUT_BUFLEN];
    char buffer_b[INPUT_BUFLEN];
    char* lastBuffer;
    char* activeBuffer;
    int activeBufferIndex;
    
    bool messageStarted;

    char outputBuffer[OUTPUT_BUFLEN];

    UnitMode mode;
    bool stream();
    bool enterConfig();
    
    char* createCmd(char const* cmd);
    char* createCmd(char const* cmd, int value);
    char* createCmd(char const* cmd, char* str);
    char* createCmd(char const* cmd, float value);
    void clearOutputBuffer();
    bool receiveStr(){return true;};
        
};

#endif