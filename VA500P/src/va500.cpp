#include "va500.h"
#include "va500-cmds.h"

#include <cstring>
#include <cstdio>
#include <stdlib.h> 

VA500P::VA500P(RangeUnits::RangeUnits rangeUnit, PressureUnits::PressureUnits pressureUnit):
_rangeUnit(rangeUnit), _pressureUnit(pressureUnit) {

    min_range = 0; 
    max_range = 100; 

    altitude = 0;
    depth = 0;
    soundSpeed = 1500.0; 
    tare = 0; 
    activeBufferIndex = 0;

    activeBuffer = buffer_a; 
    lastBuffer = buffer_b; 

    mode = uninit;
    

    memset(buffer_a, 0, INPUT_BUFLEN * (sizeof buffer_a[0]) );    
    memset(buffer_b, 0, INPUT_BUFLEN * (sizeof buffer_b[0]) );    
}

bool VA500P::setup() {
    bool result; 
    result = enterConfig();
    result &= setRangeUnits(_rangeUnit);        
    result &= setPressureUnits(_pressureUnit);        
    result &= stream();
    return result;
}

float VA500P::getDepth() {
    return depth;
}

float VA500P::getAltitude() {
    return altitude;
}

bool VA500P::enterConfig() {
    if (mode!=config) {    
        clearOutputBuffer();
        snprintf(outputBuffer, sizeof outputBuffer, "%s\n", ENTER_SETUP_MODE_CMD);
        sendCmd(outputBuffer);
        wait_ms(2000);
        if (strchr(activeBuffer, '>') != NULL) {
            mode = config;
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
    
}

bool VA500P::stream() {
    if (mode!=streaming) {    
        clearOutputBuffer();
        sendCmd(createCmd(ENTERT_RUN_MODE_CMD));
        mode = streaming;
    } 
    return true;
}

bool VA500P::setSoundSpeed(float speedOfSound) {
    //Send speed of sound to transducer
    sendCmd(createCmd(SET_SOUNDS_SPEED_CMD,speedOfSound));
    wait_ms(2);

    //Check response 
    char tmp[8];
    snprintf(tmp, 8, "%7.2f",speedOfSound);

    if (strstr(lastBuffer, tmp ) != NULL) {
        soundSpeed = speedOfSound;
        return true;
    }
    else {
        return false;
    }
}

bool VA500P::setRangeUnits(RangeUnits::RangeUnits unit) {
    if(!enterConfig()) {
        return false; 
    }
    wait_ms(500);
    sendCmd(createCmd(SET_RANGE_UNITS_CMD,unit));
    wait_ms(500);
    stream();
    
    return true;    
}

bool VA500P::setFrequency(int hz) {
    // Enter configuration mode 
    if(!enterConfig()) {
        return false; 
    }
    wait_ms(500);
    // Set sampling frequency
    switch (hz) {
        case 1:
            sendCmd(createCmd(SET_UNIT_MODE_CMD,(char *)"M1"));
            break;
        case 2:
            sendCmd(createCmd(SET_UNIT_MODE_CMD,(char *)"M2"));
            break;
        case 4:
            sendCmd(createCmd(SET_UNIT_MODE_CMD,(char *)"M4"));
            break;
        default:
            sendCmd(createCmd(SET_UNIT_MODE_CMD,(char *)"M2"));
            break;            
    }
    wait_ms(500);
    // Start measurements; 
    stream();
    return true;
}

bool VA500P::setPressureUnits(PressureUnits::PressureUnits unit) {
    if(!enterConfig()) {
        return false; 
    }
    
    wait_ms(500);    
    sendCmd(createCmd(SET_PRESSURE_UNITS_CMD,unit));
    wait_ms(500);    
    
    stream();
    return true;
}



bool VA500P::setTare() {
    return true;
}

void VA500P::addChar(char data) {
    if (data=='$' || data=='>') { 
        // Beginning of sentence received 
        activeBufferIndex = 0;
    }

    if (activeBufferIndex < (INPUT_BUFLEN-1)) {
        // Add element to buffer 
        activeBuffer[activeBufferIndex] = data; 
        activeBufferIndex++;
    }

    if (data=='\n') {
        // Switch buffers 
        if (activeBuffer == buffer_a) {
            activeBuffer = buffer_b;
            lastBuffer = buffer_a;
        }
        else if (activeBuffer == buffer_b) {
            activeBuffer = buffer_a; 
            lastBuffer = buffer_b;
        }
        activeBufferIndex = 0;   
        newMessageReceived = true; 
    }
  
}

bool VA500P::processBuffer() {
    // Check for NMEA start character
    char* idx = strchr(lastBuffer,'$');
    if (idx==NULL) return false;

    if (strstr(lastBuffer, "PRVAT") == NULL) {
        // Wrong NMEA sentence type
        return false;
    }
    
    // Check Checksum
    idx = strchr(idx,'*')+1;
    if (idx==NULL) return false;    
    int checksum = calcchecksum(lastBuffer);
    if (checksum != strtol(idx, NULL, 16) ) {
        return false; 
    }

    // Parse VALEPORT PRVAT string 
    idx = strchr(lastBuffer,',')+1;
    if (idx==NULL) return false;
    float temp_altitude = atof(idx);
    char altUnit;
    idx = strchr(idx,',')+1;
    if (idx==NULL) return false;
    memcpy(&altUnit, idx , 1 );

    idx = strchr(idx,',')+1;
    if (idx==NULL) return false;
    float temp_depth = atof(idx);
    idx = strchr(idx,',')+1;
    if (idx==NULL) return false;
    char pUnit[7];
    int length = strchr(idx,'*')-idx;
    if (length > 7) {
        //port->printf("Buffer underflown");
        return false;
    }
    memcpy(&pUnit, idx, length);
    
    // TODO: Units can be multiple characters, not only single value 
    if (altUnit != RangeUnits::range_literals[_rangeUnit][0]){
        setRangeUnits(_rangeUnit);
        return false;
    }

    if (strstr(pUnit, PressureUnits::pressure_literals[_pressureUnit]) ==NULL){
        setPressureUnits(_pressureUnit);        
        return false; 
    }
    depth = temp_depth;
    altitude = temp_altitude;
    return true;


}


char* VA500P::createCmd(char const* cmd) {
    clearOutputBuffer();
    snprintf(outputBuffer, sizeof outputBuffer, "%s\n", cmd);
    return outputBuffer;
}


char* VA500P::createCmd(char const* cmd, int value) {
    clearOutputBuffer();
    snprintf(outputBuffer, sizeof outputBuffer, "%s;%i\n", cmd, value); 
    return outputBuffer;    
}

char* VA500P::createCmd(char const* cmd, char* str) {
    clearOutputBuffer();
    snprintf(outputBuffer, sizeof outputBuffer, "%s;%s\n", cmd, str); 
    return outputBuffer;    
}

char* VA500P::createCmd(char const* cmd, float value) {
    clearOutputBuffer();
    snprintf(outputBuffer, sizeof outputBuffer, "%s;%5.2f\n", cmd, value); 
    return outputBuffer;    
}

void VA500P::clearOutputBuffer() {
    memset(outputBuffer, 0, OUTPUT_BUFLEN * (sizeof outputBuffer[0]) );    
}

int VA500P::calcchecksum(char* array) {
    const char* start_idx = strchr(array,'$')+1;
    const char* end_idx = strchr(array,'*');

    if (start_idx==NULL || end_idx==NULL) {
        return 0;
    }
    int checksum =0;
    for (char* p = (char*) start_idx; p<end_idx; p++) {
        checksum ^= (int) *p;
    }
    return checksum;
    
}