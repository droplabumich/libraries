#include "va500-desktop.h"
#include <iostream>
#include <thread>

void pollSerialPort(desktopVA500P* va500) {
    
    char tmp;
    int res;
    
    while(1) {
        res = va500->port.read(&tmp,1);
        if (res !=0 && res!=-1) {
            va500->addChar(tmp);   
        } else {
            std::this_thread::sleep_for(std::chrono::milliseconds(2));;
        } 

    }  
}

int main(int argc, char **argv) {

    desktopVA500P va500("/dev/ttyUSB0",115200, RangeUnits::seconds); 

    std::thread t1(pollSerialPort, &va500);

    va500.wait_ms(1000);
    va500.setup();
    va500.wait_ms(500);
    va500.setFrequency(4);
    
    while(1) {
        if (va500.newMessageReceived) {
            va500.newMessageReceived = false;
            std::cout<<"Buffer: "<<va500.getLastBuffer();
                
            if (va500.processBuffer()) {
                std::cout<< " Depth: "<<va500.getDepth()<<" Altitude: "<< va500.getAltitude() <<std::endl;    
            }       
        } else {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));;
            
        }
    } 

    
}