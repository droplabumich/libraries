/***********************************
This is the Adafruit GPS library - the ultimate GPS library
for the ultimate GPS module!
Tested and works great with the Adafruit Ultimate GPS module
using MTK33x9 chipset
    ------> http://www.adafruit.com/products/746
Pick one up today at the Adafruit electronics shop 
and help support open source hardware & software! -ada
Adafruit invests time and resources providing this open source code, 
please support Adafruit and open-source hardware by purchasing 
products from Adafruit!
Written by Limor Fried/Ladyada  for Adafruit Industries.  
BSD license, check license.txt for more information
All text above must be included in any redistribution

Modified by Eduardo Iscar, 2017 for EM506 GPS and mbed. 
****************************************/





#ifndef _EM506_GPS_H
#define _EM506_GPS_H

#include "mbed.h"
#include <stdint.h>
#include <math.h>
#include <ctype.h>

// different commands to set the update rate from once a second (1 Hz) to 10 times a second (10Hz)
// to generate your own sentences, check out the MTK command datasheet and use a checksum calculator
// such as the awesome http://www.hhhh.org/wiml/proj/nmeaxor.html
#define SET_GSA_OFF "$PSRF103,02,00,00,01*26\r\n"
#define SET_RMC_OFF "$PSRF103,04,00,00,01*20\r\n"
#define SET_GSV_OFF "$PSRF103,03,00,00,01*27\r\n"


// how long to wait when we're looking for a response
#define MAXWAITSENTENCE 5



class EM506_GPS {
 public:
  void begin(int baud); 

  EM506_GPS(RawSerial * ser);

  char *lastNMEA(void);
  bool newNMEAreceived();
  void common_init(void);
  void sendCommand(const char *);

  uint8_t parseHex(char c);

  void read_callback();
  char process_char(char c);
  bool parse(char * nmea);


  uint8_t hour, minute, seconds, year, month, day;
  uint16_t milliseconds;
  double latitude, longitude, geoidheight, altitude;
  double speed, angle, magvariation, HDOP;
  char lat, lon, mag;
  bool fix;
  uint8_t fixquality, satellites;

 private:
  
  RawSerial * gpsSerial;
};

#endif