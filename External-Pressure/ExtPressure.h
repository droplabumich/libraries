


/***********************************

****************************************/
#include "mbed.h"
#include <stdint.h>
#include <math.h>
#include <ctype.h>

#ifndef _EXT_PRESSURE_H
#define _EXT_PRESSURE_H




// how long to wait when we're looking for a response
#define MAXWAITSENTENCE 5



class EXTERNAL_PRESSURE {
 public:
  void begin(int baud); 

  EXTERNAL_PRESSURE(RawSerial * ser);

  char *lastNMEA(void);
  bool newNMEAreceived();
  void common_init(void);
  void sendCommand(char *);

  uint8_t parseHex(char c);

  void read_callback();
  char process_char(char c);
  bool parse(char * nmea);


  float pressure, temperature;

 private:
  
  RawSerial * gpsSerial;
};

#endif