
#include "ExtPressure.h"

// how long are max NMEA lines to parse?
#define MAXLINELENGTH 120

// we double buffer: read one line in and leave one for the main program
volatile char line1_ext[MAXLINELENGTH];
volatile char line2_ext[MAXLINELENGTH];
// our index into filling the current line
volatile uint16_t lineidx_ext=0;
// pointers to the double buffers
volatile char *currentline_ext;
volatile char *lastline_ext;
volatile bool recvdflag_ext;
volatile bool inStandbyMode_ext;


bool EXTERNAL_PRESSURE::parse(char *nmea) {
  // first look if we even have one
  if (nmea[strlen(nmea)-4] == '*') {
    uint16_t sum = parseHex(nmea[strlen(nmea)-3]) * 16;
    sum += parseHex(nmea[strlen(nmea)-2]);
    
    // check checksum 
    for (uint8_t i=1; i < (strlen(nmea)-4); i++) {
      sum ^= nmea[i];
    }
    if (sum != 0) {
      // bad checksum :(
      //_port->printf("Bad checksum \n\r");
      return false;
    }
  }
  int ndata =2;
  char *p = nmea;
    //_port->printf("Number of AD data: %i",ndata);
  int data_id;
  p = strchr(p, ',')+1;
  data_id = atoi(p);
  p = strchr(p, ',')+1;
  pressure = atof(p);
  p = strchr(p, ',')+1;
  data_id = atoi(p);
  p = strchr(p, ',')+1;
  temperature = atof(p);    
        //_port->printf("Data id %i \n\r",data_id_array[i]);
        //_port->printf("Data cont %f \n\r",data_array[i]);
  return true;
}

void EXTERNAL_PRESSURE::read_callback(){
    while (gpsSerial->readable()) {
        process_char(gpsSerial->getc());
    }
}


char EXTERNAL_PRESSURE::process_char(char c) {

  if (c == '$') {
    currentline_ext[lineidx_ext] = 0;
    lineidx_ext = 0;
  }
  if (c == '\n') {
    currentline_ext[lineidx_ext] = 0;

    if (currentline_ext == line1_ext) {
      currentline_ext = line2_ext;
      lastline_ext = line1_ext;
    } else {
      currentline_ext = line1_ext;
      lastline_ext = line2_ext;
    }

    lineidx_ext = 0;
    recvdflag_ext = true;
  }

  currentline_ext[lineidx_ext++] = c;
  if (lineidx_ext >= MAXLINELENGTH)
    lineidx_ext = MAXLINELENGTH-1;

  return c;
}

EXTERNAL_PRESSURE::EXTERNAL_PRESSURE (RawSerial *ser)
{
    common_init();     // Set everything to common state, then...
    gpsSerial = ser; // ...override gpsSwSerial with value passed.

}

// Initialization code used by all constructor types
void EXTERNAL_PRESSURE::common_init(void) {
  gpsSerial = NULL;
  recvdflag_ext   = false;
  lineidx_ext     = 0;
  currentline_ext = line1_ext;
  lastline_ext    = line2_ext;

  
  pressure = 0;
  temperature = 0;                             // uint16_t
  }

void EXTERNAL_PRESSURE::begin(int baud)
{
  gpsSerial->baud(baud);
  wait_ms(10);
}

void EXTERNAL_PRESSURE::sendCommand(char *str) {
  gpsSerial->printf("%s",str);
}

bool EXTERNAL_PRESSURE::newNMEAreceived(void) {
  return recvdflag_ext;
}


char *EXTERNAL_PRESSURE::lastNMEA(void) {
  recvdflag_ext = false;
  return (char *)lastline_ext;
}

// read a Hex value and return the decimal equivalent
uint8_t EXTERNAL_PRESSURE::parseHex(char c) {
    if (c < '0')
      return 0;
    if (c <= '9')
      return c - '0';
    if (c < 'A')
       return 0;
    if (c <= 'F')
       return (c - 'A')+10;
}



